package ge.gi.logistic_tbc.ui.domain.pages.create_order.bottom_sheet

interface OnCardClickListener {
    fun onCardClick(position : Int)
    fun onAddCardClick(){}
}