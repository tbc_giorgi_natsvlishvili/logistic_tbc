package ge.gi.logistic_tbc.ui.domain.pages.create_order.bottom_sheet

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.ui.domain.models.UserModel
import kotlinx.android.synthetic.main.item_credit_card_chooser.view.*
import kotlinx.android.synthetic.main.item_no_card_found.view.*

class ChooserItemsRecyclerViewAdapter(
    private val items: MutableList<UserModel.CreditCard>,
    private val itemClick: OnCardClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val CODE_CARD = 1
        const val CODE_NO_CARD = 3
    }

    override fun getItemViewType(position: Int)  = if (items.size > 0) CODE_CARD else CODE_NO_CARD


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            CODE_CARD -> {
                CardViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_credit_card, parent, false)
                )
            }
            else -> {
                NoCardFoundViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_no_card_found, parent, false)
                )
            }
        }
    }

    override fun getItemCount() = if(items.size > 0) items.size else 1

    override fun onBindViewHolder(holderCard: RecyclerView.ViewHolder, position: Int) {
        if (holderCard is CardViewHolder) holderCard.onBind()
        else if (holderCard is NoCardFoundViewHolder) holderCard.onBind()
    }

    inner class CardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val card = items[adapterPosition]

            itemView.cardHolder.text = card.holder
            val cardID = "**** ${card.cardID!!.substring(card.cardID!!.length - 4)}"
            itemView.cardNumber.text = cardID
            itemView.setOnClickListener {
                itemClick.onCardClick(adapterPosition)
            }
        }
    }

    inner class NoCardFoundViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            itemView.addCardBtn.setOnClickListener {
                itemClick.onAddCardClick()
            }
        }
    }

}
