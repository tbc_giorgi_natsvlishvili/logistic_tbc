package ge.gi.logistic_tbc.ui.domain.pages.create_order.adapter

interface OnVehicleClick {
    fun onVehicleClick(position: Int){}
}