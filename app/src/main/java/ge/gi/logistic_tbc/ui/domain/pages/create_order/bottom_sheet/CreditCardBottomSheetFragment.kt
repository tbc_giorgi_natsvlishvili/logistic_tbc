package ge.gi.logistic_tbc.ui.domain.pages.create_order.bottom_sheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.ui.domain.models.UserModel
import kotlinx.android.synthetic.main.layout_bottom_sheet_card.view.*


class CreditCardBottomSheetFragment(private val items: ArrayList<UserModel.CreditCard>,
    private val onCardClickListener: OnCardClickListener
) : BottomSheetDialogFragment() {


    private lateinit var chooserAdapter: ChooserItemsRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.layout_bottom_sheet_card, container, false)
        chooserAdapter = ChooserItemsRecyclerViewAdapter(items, object : OnCardClickListener {
            override fun onCardClick(position: Int) {
                dismiss()
                onCardClickListener.onCardClick(position)
            }
            override fun onAddCardClick() {
                dismiss()
                onCardClickListener.onAddCardClick()
            }
        })

        view.cardItemsRecyclerView.apply {
            layoutManager = LinearLayoutManager(view.context)
            adapter = chooserAdapter
        }

        return view
    }
}