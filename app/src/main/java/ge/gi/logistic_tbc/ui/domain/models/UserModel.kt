package ge.gi.logistic_tbc.ui.domain.models

class UserModel {
    var userId: String? = null
    var fullName: String? = null
    var email: String? = null
    var phoneNumber: String? = null
    var imgUrl: String? = null
    var billingAddress: String? = null
    var creditCard: MutableList<CreditCard>? = null
    var numberOfOrders = 0
    var numberOfDeliveredOrders = 0
    var numberOfCancelledOrders = 0


    class CreditCard {
        companion object {
            fun getFormattedCardID(cardID : String) = "**** **** **** ${cardID.substring(cardID.length - 4)}"
        }
        var cardID: String? = null
        var expireDate: String? = null
        var holder: String? = null
        var ccv: String? = null
    }
}