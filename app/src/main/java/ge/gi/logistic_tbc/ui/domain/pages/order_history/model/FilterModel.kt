package ge.gi.logistic_tbc.ui.domain.pages.order_history.model

class FilterModel(
    var statusCode : Int? = null,
    var title: String,
    var checked: Boolean = false

)