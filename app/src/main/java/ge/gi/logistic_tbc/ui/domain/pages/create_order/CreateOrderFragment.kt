package ge.gi.logistic_tbc.ui.domain.pages.create_order

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.extensions.setColorByEditTextState
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.tools.OnDialogCallBack
import ge.gi.logistic_tbc.tools.Tools
import ge.gi.logistic_tbc.ui.BaseFragment
import ge.gi.logistic_tbc.ui.authorisation.User
import ge.gi.logistic_tbc.ui.domain.DomainActivity
import ge.gi.logistic_tbc.ui.domain.models.Order
import ge.gi.logistic_tbc.ui.domain.models.UserModel
import ge.gi.logistic_tbc.ui.domain.pages.create_order.adapter.ChooseVehicleTypeAdapter
import ge.gi.logistic_tbc.ui.domain.pages.create_order.adapter.OnVehicleClick
import ge.gi.logistic_tbc.ui.domain.pages.create_order.bottom_sheet.CreditCardBottomSheetFragment
import ge.gi.logistic_tbc.ui.domain.pages.create_order.bottom_sheet.OnCardClickListener
import ge.gi.logistic_tbc.ui.domain.pages.create_order.model.Vehicle
import ge.gi.logistic_tbc.ui.domain.pages.user_profile.AddCreditCardActivity
import ge.gi.logistic_tbc.ui.domain.pages.user_profile.UserProfileFragment
import kotlinx.android.synthetic.main.fragment_create_order.*
import kotlinx.android.synthetic.main.fragment_create_order.view.*
import kotlinx.android.synthetic.main.fragment_create_order.view.productName
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CreateOrderFragment : BaseFragment() {

    private lateinit var chooseVehicleTypeAdapter: ChooseVehicleTypeAdapter
    private val vehicles = mutableListOf<Vehicle>()
    private var vehiclePosition = 0

    // User
    private lateinit var user: UserModel
    private val creditItemList = ArrayList<UserModel.CreditCard>()

    // Bottom Sheet
    private lateinit var creditCardBottomSheetFragment: CreditCardBottomSheetFragment


    private lateinit var order: Order
    private var tempoNumber = 0

    // add Comment
    var comment = ""

    private var creditCard: UserModel.CreditCard? = null

    companion object {
        const val ADD_ADDRESS_FROM_CODE = 99
        const val ADD_ADDRESS_DESTINATION_CODE = 123
        const val ADD_CARD_INTENT_CODE = 13

    }

    override fun getLayoutResource() = R.layout.fragment_create_order

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        setLocalData()
        initRV()
        fetchUserFromDB()
        setListeners()
    }

    private fun setLocalData() {
        vehicles.add(
            Vehicle(
                R.mipmap.ic_vehicle_scooter,
                getString(R.string.vehicle_type_scooter),
                true
            )
        )
        vehicles.add(Vehicle(R.mipmap.ic_vehicle_mini, getString(R.string.vehicle_type_mini)))
        vehicles.add(
            Vehicle(
                R.mipmap.ic_vehicle_mini_plus,
                getString(R.string.vehicle_type_mini_plus)
            )
        )
        vehicles.add(Vehicle(R.mipmap.ic_vehicle_heavy, getString(R.string.vehicle_type_heavy)))
    }

    private fun initRV() {
        itemView!!.chooseVehicleRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        chooseVehicleTypeAdapter = ChooseVehicleTypeAdapter(vehicles, vehicleCallBack)
        itemView!!.chooseVehicleRV.adapter = chooseVehicleTypeAdapter
    }

    private fun fetchUserFromDB() {
        if (User.userFetched) {
            user = User.userModel
            if (user.creditCard != null)
                creditItemList.addAll(user.creditCard!!.toList())
        }
    }

    private fun showChooserDialog() {
        creditCardBottomSheetFragment =
            CreditCardBottomSheetFragment(creditItemList, object : OnCardClickListener {

                override fun onCardClick(position: Int) {
                    creditCard = creditItemList[position]
                    itemView!!.cardHolder.text = creditCard!!.holder
                    itemView!!.cardNumber.text =
                        UserModel.CreditCard.getFormattedCardID(creditCard!!.cardID!!)
                }

                override fun onAddCardClick() {
                    val intent =
                        Intent(activity as DomainActivity, AddCreditCardActivity::class.java)
                    intent.putExtra(FireBase.DOCUMENT_ID, User.documentID)
                    startActivityForResult(intent, ADD_CARD_INTENT_CODE)
                }
            })
        creditCardBottomSheetFragment.show(
            childFragmentManager,
            "Choose Credit Card Dialog Fragment"
        )
    }

    private val vehicleCallBack = object : OnVehicleClick {
        override fun onVehicleClick(position: Int) {
            itemView!!.chooseVehicleRV.scrollToPosition(position)
            vehiclePosition = position
        }
    }

    private fun setListeners() {
        itemView!!.productWeight.setOnFocusChangeListener { _, isFocused ->
            itemView!!.kilogram.setColorByEditTextState(isFocused)
        }

        itemView!!.commentDialogTV.setOnClickListener {
            Tools.commentDialog(context!!, comment, object : OnDialogCallBack {
                override fun onNoteAdded(text: String) {
                    comment = text
                }
            })
        }

        itemView!!.from.setOnClickListener {
            val intent = Intent(activity as DomainActivity, SearchAddressActivity::class.java)
            startActivityForResult(intent, ADD_ADDRESS_FROM_CODE)
            (activity as DomainActivity).overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }

        itemView!!.to.setOnClickListener {
            val intent = Intent(activity as DomainActivity, SearchAddressActivity::class.java)
            startActivityForResult(intent, ADD_ADDRESS_DESTINATION_CODE)
            (activity as DomainActivity).overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }

        itemView!!.cardChooser.setOnClickListener {
            creditItemList.clear()
            fetchUserFromDB()
            showChooserDialog()
        }

        itemView!!.createOrderBtn.setOnClickListener {
            validateFields()
        }
    }

    private fun validateFields() {
        val productName = itemView!!.productName.text.toString()
        val productQuantity = itemView!!.productQuantity.text.toString()
        val productWeight = itemView!!.productWeight.text.toString()
        val addressFrom = itemView!!.addressFrom.text.toString()
        val addressDestination = itemView!!.addressDestination.text.toString()

        if (productName.isEmpty() || productQuantity.isEmpty() || productWeight.isEmpty() || addressFrom.isEmpty() || addressDestination.isEmpty()) {
            Tools.dialog(
                context!!,
                getString(R.string.fields_is_empty_title),
                getString(R.string.fields_is_empty_desc),
                Tools.DIALOG_ALERT,
                getString(R.string.try_again)
            )
            return
        }

        if (creditCard == null) {
            Tools.dialog(
                context!!,
                getString(R.string.billing_error),
                getString(R.string.billing_error_desc),
                Tools.DIALOG_ALERT,
                getString(R.string.ok)
            )
            return
        }

        commitOrder(
            productName,
            productQuantity,
            productWeight,
            addressFrom,
            addressDestination,
            creditCard!!
        )
    }

    @SuppressLint("SimpleDateFormat")
    private fun commitOrder(
        productName: String,
        productQuantity: String,
        productWeight: String,
        addressFrom: String,
        addressDestination: String,
        creditCard: UserModel.CreditCard
    ) {

        val order = Order().apply {
            userId = FireBase.auth.currentUser!!.uid
            orderID = "ORD${System.currentTimeMillis()}"
            orderStatus = getString(R.string.pending)
            orderStatusCode = 1
//            val dateFormatter: DateFormat = SimpleDateFormat("dd/MM/yy HH:mm:ss")
//            val calendar: Calendar = Calendar.getInstance()
//            createOrderDate = dateFormatter.format(calendar.time)

            createOrderDate = System.currentTimeMillis().toString()
            shippingLocation = addressFrom
            destination = addressDestination

            product = Order.Product().apply {
                name = productName
                price = 1200
                weight = productWeight.toInt()
                quantity = productQuantity.toInt()
            }

            driver = Order.Driver().apply {
                vehicle = vehicles[vehiclePosition].vehicleType
            }

            attachedCreditCard = creditCard
        }

        FireBase.db.collection(FireBase.TABLE_NAME_ORDER)
            .add(order)
            .addOnSuccessListener { documentReference ->
                d(
                    "OrderCreationSuccess",
                    "DocumentSnapshot written with ID: ${documentReference.id}"
                )
                Tools.dialog(
                    context!!,
                    getString(R.string.order_creation_success),
                    getString(R.string.order_creation_success_desc),
                    Tools.DIALOG_SUCCESS,
                    getString(R.string.ok)
                )
                clearFieldsAfterUpload()

            }
            .addOnFailureListener { e ->
                d("success", "DocumentSnapshot written with ID: $e")
                Tools.dialog(
                    context!!,
                    getString(R.string.order_creation_failure),
                    getString(R.string.order_creation_failure_desc),
                    Tools.DIALOG_ERROR,
                    getString(R.string.try_again)
                )
            }
    }

    private fun clearFieldsAfterUpload() {
        productName.setText("")
        productQuantity.setText("")
        productWeight.setText("")
        addressFrom.text = ""
        addressDestination.text = ""
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    ADD_ADDRESS_FROM_CODE -> {
                        if (data != null) {
                            val address =
                                data.extras!!.getString(SearchAddressActivity.ADDRESS_EXTRA_KEY)
                            itemView!!.addressFrom.text = address
                            data.removeExtra(SearchAddressActivity.ADDRESS_EXTRA_KEY)
                        }
                    }

                    ADD_ADDRESS_DESTINATION_CODE -> {
                        if (data != null) {
                            val address =
                                data.extras!!.getString(SearchAddressActivity.ADDRESS_EXTRA_KEY)
                            itemView!!.addressDestination.text = address
                            data.removeExtra(SearchAddressActivity.ADDRESS_EXTRA_KEY)
                        }
                    }

                    ADD_CARD_INTENT_CODE -> {
                        if (data != null) {
                            val boolean =
                                data.extras!!.getBoolean(UserProfileFragment.IS_CARD_ADDED, false)
                            if (boolean)
                                User.getUserInfo()
                            data.removeExtra(UserProfileFragment.IS_CARD_ADDED)
                        }
                    }
                }
            }

            Activity.RESULT_CANCELED -> {
                d("test", "canceled")
            }
        }
    }

}