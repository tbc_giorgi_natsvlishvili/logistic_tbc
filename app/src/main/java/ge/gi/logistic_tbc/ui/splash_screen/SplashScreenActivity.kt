package ge.gi.logistic_tbc.ui.splash_screen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log.d
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.tools.Tools
import ge.gi.logistic_tbc.ui.authorisation.LoginActivity
import ge.gi.logistic_tbc.ui.domain.DomainActivity

class SplashScreenActivity : AppCompatActivity() {

    private val handler = Handler()
    private val runnable = Runnable {
        chooseActivityByUserCondition()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
    }


    override fun onResume() {
        super.onResume()
        freeze()
    }

    override fun onPause() {
        super.onPause()
        removeCall()
    }

    private fun freeze() {
        handler.postDelayed(runnable, 2000)
    }

    private fun removeCall() {
        handler.removeCallbacks(runnable)
    }


    private fun chooseActivityByUserCondition() {
        if(FireBase.auth.currentUser != null)
            Tools.startNewActivity(this, DomainActivity(),true)
        else
            Tools.startNewActivity(this, LoginActivity(),true)
    }
}