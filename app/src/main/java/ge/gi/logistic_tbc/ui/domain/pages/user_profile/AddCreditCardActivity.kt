package ge.gi.logistic_tbc.ui.domain.pages.user_profile

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.firebase.FireBaseCallBack
import ge.gi.logistic_tbc.tools.OnDialogCallBack
import ge.gi.logistic_tbc.tools.Tools
import ge.gi.logistic_tbc.ui.domain.models.UserModel
import kotlinx.android.synthetic.main.activity_add_credit_card.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class AddCreditCardActivity : AppCompatActivity() {

    private lateinit var documentId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_credit_card)

        getIntentExtras()
        initToolbar()
        setListeners()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitleTV.text = getString(R.string.add_credit_card)
    }

    private fun validateFields() {
        val holder: String = addCardHolder.text.toString()
        val number: String = addCardNumber.text.toString()
        val expireDate: String = addExpireDate.text.toString()
        val ccv: String = addCCV.text.toString()

        if (holder.isEmpty() || number.isEmpty() || expireDate.isEmpty() || ccv.isEmpty()) {
            Tools.dialog(
                this@AddCreditCardActivity,
                getString(R.string.fields_is_empty_title),
                getString(R.string.fields_is_empty_desc),
                Tools.DIALOG_ALERT,
                getString(R.string.try_again)
            )
            return
        }

        /**
         * აქ გვინდა Card Number ის და CCV ის ვალიდაცია
         */

        addCreditCardToDatabase(UserModel.CreditCard().apply {
            this.holder = holder
            cardID = number
            this.expireDate = expireDate
            this.ccv = ccv
        })
    }

    private fun getIntentExtras() {
        documentId = intent.extras!!.get(FireBase.DOCUMENT_ID) as String
        intent.removeExtra(FireBase.DOCUMENT_ID)
    }

    private fun addCreditCardToDatabase(creditCard: UserModel.CreditCard) {
        val params = mutableMapOf<String, Any>()
        params[FireBase.CREDIT_CARD] = creditCard

        FireBase.addCreditCard(
            FireBase.TABLE_NAME_USERS,
            documentId,
            FireBase.CREDIT_CARD,
            creditCard,
            object : FireBaseCallBack {
                override fun onFailureUpdate(error: String) {
                    Tools.dialog(
                        this@AddCreditCardActivity,
                        getString(R.string.error_adding_credit_card),
                        error,
                        Tools.DIALOG_ERROR,
                        getString(R.string.try_again)
                    )
                }

                override fun onSuccessfulUpdate() {
                    Tools.dialog(
                        this@AddCreditCardActivity,
                        getString(R.string.credit_card_added),
                        getString(R.string.credit_card_added_desc),
                        Tools.DIALOG_SUCCESS,
                        getString(R.string.ok),
                       onDialogCallBack =  object : OnDialogCallBack {
                            override fun okButtonPressed() {
                                finishActivity()
                            }
                        }
                    )
                }
            }
        )
    }

    private fun finishActivity() {
        intent.putExtra(UserProfileFragment.IS_CARD_ADDED, true)
        setResult(Activity.RESULT_OK, intent)
        onBackPressed()
    }

    private fun setListeners() {
        addCreditCardBtn.setOnClickListener {
            validateFields()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
}