package ge.gi.logistic_tbc.ui.domain.models


class Order() {

    var userId: String? = null
    var orderID: String? = null
    var orderStatus: String? = null
    var orderStatusCode: Int? = null
    var createOrderDate: String? = null
    var deliverDate: String? = null
    var shippingLocation: String? = null
    var destination: String? = null
    var product = Product()
    var driver = Driver()

    // Payment
    var attachedCreditCard: UserModel.CreditCard? = null

    class Product {
        var quantity: Int? = null
        var price: Int? = null
        var weight: Int? = null
        var name: String? = null


        fun getDollarPrice() = "$ $price"
    }

    class Driver {
        var driverFullName: String? = null
        var driverImg: String? = null
        var vehicle: String? = null
        var vehicleNumber: String? = null
        var driverRating: String? = null
        var driverPhoneNumber: String? = null
    }

}