package ge.gi.logistic_tbc.ui.domain.pages.order_history.adapter

import ge.gi.logistic_tbc.ui.domain.pages.order_history.model.FilterModel

interface OnFilterSuggestionClickListener {
    fun onSuggestionClick(suggestion: FilterModel, statusCode : Int)
}