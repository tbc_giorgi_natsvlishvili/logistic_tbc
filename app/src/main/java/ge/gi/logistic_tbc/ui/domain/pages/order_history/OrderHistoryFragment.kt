package ge.gi.logistic_tbc.ui.domain.pages.order_history

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.firebase.FireBaseCallBack
import ge.gi.logistic_tbc.ui.BaseFragment
import ge.gi.logistic_tbc.ui.authorisation.User
import ge.gi.logistic_tbc.ui.domain.DomainActivity
import ge.gi.logistic_tbc.ui.domain.pages.order_history.adapter.FilterSuggestionAdapter
import ge.gi.logistic_tbc.ui.domain.pages.order_history.adapter.OnOrderClicked
import ge.gi.logistic_tbc.ui.domain.pages.order_history.adapter.OrderHistoryAdapter
import ge.gi.logistic_tbc.ui.domain.pages.order_history.model.FilterModel
import ge.gi.logistic_tbc.ui.domain.models.Order
import ge.gi.logistic_tbc.ui.domain.pages.order_history.adapter.OnFilterSuggestionClickListener
import ge.gi.logistic_tbc.ui.domain.pages.user_profile.UserProfileFragment
import kotlinx.android.synthetic.main.fragment_order_history.*
import kotlinx.android.synthetic.main.fragment_order_history.view.*


class OrderHistoryFragment : BaseFragment() {

    private lateinit var orderHistoryViewModel: OrderHistoryViewModel

    private lateinit var orderHistoryAdapter: OrderHistoryAdapter
    private val orders = mutableListOf<Order>()

    private lateinit var filterSuggestionAdapter: FilterSuggestionAdapter
    private lateinit var suggestions: List<FilterModel>
    private var suggestion: FilterModel? = null

    override fun getLayoutResource() = R.layout.fragment_order_history

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        setData()
        initViewModel()
        initRVs()
        setListeners()
        getData()
    }

    private fun getData() {
        orderHistoryViewModel.getData()

        orderHistoryViewModel.order.observe(this, Observer {
            (activity as DomainActivity).isRefreshing(false)
            orders.clear()
            orders.addAll(it)
            ordersCountLogic()
            orderHistoryAdapter.addItemsInOrdersFull(it)
            orderHistoryAdapter.notifyDataSetChanged()
            noOrdersFoundIllustrationVisibility()
        })
    }

    private fun setData() {
        suggestions = listOf(
            FilterModel(2, getString(R.string.on_the_way)),
            FilterModel(0, getString(R.string.delivered)),
            FilterModel(1, getString(R.string.pending)),
            FilterModel(3, getString(R.string.canceled_by_user)),
            FilterModel(4, getString(R.string.canceled_by_driver))
        )
    }

    private fun initViewModel() {
        orderHistoryViewModel = ViewModelProvider(this).get(OrderHistoryViewModel::class.java)
    }

    private fun initRVs() {
        itemView!!.filterSuggestionsRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        filterSuggestionAdapter = FilterSuggestionAdapter(suggestions, filterSuggestionCallback)
        itemView!!.filterSuggestionsRV.adapter = filterSuggestionAdapter


        itemView!!.orderHistoryRV.layoutManager = LinearLayoutManager(context)
        orderHistoryAdapter = OrderHistoryAdapter(
            orders,
            callBack,
            itemView!!.swipeRefreshLayout,
            itemView!!.orderHistoryRV
        )
        itemView!!.orderHistoryRV.adapter = orderHistoryAdapter
    }

    private val callBack = object : OnOrderClicked {
        override fun onOrderClicked(orderId: String) {
            val intent = Intent(getRootActivity(), OrderDetailsActivity::class.java)
            intent.putExtra(OrderDetailsActivity.ORDER_ID, orderId)
            startActivity(intent)
            getRootActivity().overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }
    }

    private val originalList = mutableListOf<Order>()

    private val filterSuggestionCallback = object : OnFilterSuggestionClickListener {
        override fun onSuggestionClick(suggestion: FilterModel, statusCode: Int) {

            clearFilter.visibility = View.VISIBLE

            if (originalList.size == 0)
                originalList.addAll(orders)
            orders.clear()
            orders.addAll(originalList)

            if (suggestion.checked) {
                this@OrderHistoryFragment.suggestion = suggestion
                val filteredList = orders.filter { it.orderStatusCode == statusCode }
                orders.clear()
                orders.addAll(filteredList)
                orderHistoryAdapter.notifyDataSetChanged()

            } else {
                orders.addAll(originalList)
                orderHistoryAdapter.notifyDataSetChanged()
            }
        }

    }

    private fun noOrdersFoundIllustrationVisibility() {
        if (orders.isEmpty() && itemView!!.searchOrders.text.toString()
                .isEmpty() && suggestion == null
        )
            itemView!!.noOrderFoundContainer.visibility = View.VISIBLE
        else
            itemView!!.noOrderFoundContainer.visibility = View.GONE
    }


    fun getRootActivity() = activity as DomainActivity

    fun ordersCountLogic() {
        var numberOfOrders = 0
        var numberOfDeliveredOrders = 0
        var numberOfCancelledOrders = 0
        orders.forEach {
            if (it.orderStatusCode == 0)
                numberOfDeliveredOrders++
            if (it.orderStatusCode == 3)
                numberOfCancelledOrders++

            numberOfOrders++
        }


        val params = mutableMapOf<String, Any>()
        params["numberOfCancelledOrders"] = numberOfCancelledOrders
        params["numberOfDeliveredOrders"] = numberOfDeliveredOrders
        params["numberOfOrders"] = numberOfOrders

        FireBase.update(
            FireBase.TABLE_NAME_USERS,
            User.documentID,
            params,
            object : FireBaseCallBack {

            }
        )

    }

    private fun setListeners() {
        itemView!!.swipeRefreshLayout.setOnRefreshListener {
            clearFilter(true)
            swipeRefreshLayout.isRefreshing = false
            (activity as DomainActivity).isRefreshing(true)
            originalList.clear()
            orderHistoryViewModel.getData()
        }

        itemView!!.createOrderBtn.setOnClickListener {
            getRootActivity().navigate(0)
        }

        itemView!!.clearFilter.setOnClickListener {
            clearFilter(true)

        }

        filterOrders()

    }

    private fun clearFilter(clearText: Boolean) {
        orders.clear()
        orders.addAll(originalList)
        filterSuggestionAdapter.clearAll()

        if (clearText) {
            itemView!!.searchOrders.setText("")
        }

        clearFilter.visibility = View.INVISIBLE
    }

    private fun filterOrders() {
        itemView!!.searchOrders.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                clearFilter(false)
                clearFilter.visibility = View.VISIBLE
                orderHistoryAdapter.filter.filter(p0)
            }

        })
    }
}