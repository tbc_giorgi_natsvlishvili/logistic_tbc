package ge.gi.logistic_tbc.ui.domain.pages.order_history.adapter


interface OnOrderClicked {
    fun onOrderClicked(orderId: String)
}