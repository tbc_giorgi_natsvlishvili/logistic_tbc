package ge.gi.logistic_tbc.ui.domain

import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.onesignal.OneSignal
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.extensions.refreshing
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.tools.Tools
import ge.gi.logistic_tbc.ui.authorisation.User
import ge.gi.logistic_tbc.ui.domain.adapter.ViewPagerAdapter
import ge.gi.logistic_tbc.ui.domain.pages.create_order.CreateOrderFragment
import ge.gi.logistic_tbc.ui.domain.pages.order_history.OrderHistoryFragment
import ge.gi.logistic_tbc.ui.domain.pages.user_profile.UserProfileFragment
import kotlinx.android.synthetic.main.activity_domain.*
import kotlinx.android.synthetic.main.layout_loader.*


class DomainActivity : AppCompatActivity() {

    private lateinit var viewPagerAdapter: ViewPagerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_domain)

        initBottomNav()
        setExternalUIdOnOneSignal()
        User.getUserInfo()
    }

    private fun initBottomNav() {
        val fragments = mutableListOf<Fragment>()

        fragments.add(CreateOrderFragment())
        fragments.add(OrderHistoryFragment())
        fragments.add(UserProfileFragment())


        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager, fragments)
        viewPager.offscreenPageLimit = 2
        viewPager.adapter = viewPagerAdapter

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                bottomNav.menu.getItem(position).isChecked = true
            }
        })

        bottomNav.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.fragment_order -> viewPager.currentItem = 0
                R.id.fragment_order_history -> viewPager.currentItem = 1
                R.id.fragment_user_info -> viewPager.currentItem = 2
            }
            true
        }
    }

    fun isRefreshing(isRefreshing: Boolean) {
        loaderLayout.refreshing(isRefreshing)
    }

    private fun setExternalUIdOnOneSignal() {
        val externalUserId = FireBase.auth.currentUser!!.uid
        "FireBaseUI" // You will supply the external user id to the OneSignal SDK

        // Setting External User Id with Callback Available in SDK Version 3.13.0+

        // Setting External User Id with Callback Available in SDK Version 3.13.0+
        OneSignal.setExternalUserId(
            externalUserId
        ) { results -> // The results will contain push and email success statuses
            OneSignal.onesignalLog(
                OneSignal.LOG_LEVEL.VERBOSE,
                "Set external user id done with results: $results"
            )

            // Push can be expected in almost every situation with a success status, but
            // as a pre-caution its good to verify it exists
            if (results.has("push") && results.getJSONObject("push").has("success")) {
                val isPushSuccess =
                    results.getJSONObject("push").getBoolean("success")
                OneSignal.onesignalLog(
                    OneSignal.LOG_LEVEL.VERBOSE,
                    "Set external user id for push status: $isPushSuccess"
                )
            }

            // Verify the email is set or check that the results have an email success status
            if (results.has("email") && results.getJSONObject("email").has("success")) {
                val isEmailSuccess =
                    results.getJSONObject("email").getBoolean("success")
                OneSignal.onesignalLog(
                    OneSignal.LOG_LEVEL.VERBOSE,
                    "Sets external user id for email status: $isEmailSuccess"
                )
            }
        }
    }


    fun navigate(page : Int){
        viewPager.currentItem = page
    }

}