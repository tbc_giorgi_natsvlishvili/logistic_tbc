package ge.gi.logistic_tbc.ui.authorisation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.firestore.FirebaseFirestore
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.extensions.chooseDrawableByEditTextState
import ge.gi.logistic_tbc.extensions.isEmailValid
import ge.gi.logistic_tbc.extensions.setPasswordVisibility
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.tools.Tools
import ge.gi.logistic_tbc.ui.domain.DomainActivity
import ge.gi.logistic_tbc.ui.domain.models.UserModel
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up.emailE
import kotlinx.android.synthetic.main.activity_sign_up.emailSuccessIV
import kotlinx.android.synthetic.main.activity_sign_up.passwordE
import kotlinx.android.synthetic.main.activity_sign_up.passwordToggleImageView
import kotlinx.android.synthetic.main.layout_loader.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var db: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        db = FirebaseFirestore.getInstance()

        initToolbar()
        listeners()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitleTV.text = getString(R.string.register_page_title)
    }

    private fun listeners() {
        emailE.isEmailValid(emailSuccessIV)

//      EditText Eye Logic Handling
//      ************************
        passwordToggleImageView.setOnClickListener {
            passwordE.tag = if (passwordE.tag == "1") "0" else "1"
            passwordToggleImageView.chooseDrawableByEditTextState(passwordE.isFocused, passwordE)
            passwordE.setPasswordVisibility()
        }
        passwordE.setOnFocusChangeListener { view, b ->
            passwordToggleImageView.chooseDrawableByEditTextState(b, view as EditText)
        }

//      repeatPassword EditText Eye Logic Handling

        repeatPasswordToggleImageView.setOnClickListener {
            repeatPasswordE.tag = if (repeatPasswordE.tag == "1") "0" else "1"
            repeatPasswordToggleImageView.chooseDrawableByEditTextState(
                repeatPasswordE.isFocused,
                repeatPasswordE
            )
            repeatPasswordE.setPasswordVisibility()
        }
        repeatPasswordE.setOnFocusChangeListener { view, b ->
            repeatPasswordToggleImageView.chooseDrawableByEditTextState(b, view as EditText)
        }
//        ***********************

        signUpBtn.setOnClickListener {
            validateFields()
        }
    }

    private fun validateFields() {
        val email = emailE.text.toString()
        val password = passwordE.text.toString()
        val repeatPassword = repeatPasswordE.text.toString()
        val fullName = fullNameE.text.toString()
        val phoneNumber = phoneE.text.toString()

        if (email.isEmpty() || password.isEmpty() || repeatPassword.isEmpty() || fullName.isEmpty() || phoneNumber.isEmpty()) {
            Tools.dialog(
                this@SignUpActivity,
                getString(R.string.fields_is_empty_title),
                getString(R.string.fields_is_empty_desc),
                Tools.DIALOG_ALERT,
                getString(R.string.try_again)
            )
            return
        }
        if (emailE.tag == "0") {
            Tools.dialog(
                this@SignUpActivity,
                getString(R.string.email_is_not_valid_title),
                getString(R.string.email_is_not_valid_desc),
                Tools.DIALOG_ERROR,
                getString(R.string.try_again)
            )
            return
        }

        if (password.length < 6) {
            Tools.dialog(
                this@SignUpActivity,
                getString(R.string.password_is_not_valid_title),
                getString(R.string.password_is_not_valid_desc),
                Tools.DIALOG_ERROR,
                getString(R.string.try_again)
            )
            return
        }


        if (password != repeatPassword) {
            Tools.dialog(
                this@SignUpActivity,
                getString(R.string.passwords_not_match_title),
                getString(R.string.passwords_not_match_desc),
                Tools.DIALOG_ERROR,
                getString(R.string.try_again)
            )
            return
        }

        signUpWithEmail(email, password, fullName, phoneNumber)
    }



    private fun signUpWithEmail(email: String, password: String, fullName : String, phoneNumber : String) {
        loaderLayout.visibility = View.VISIBLE
        FireBase.auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                loaderLayout.visibility = View.GONE
                val user = UserModel().apply {
                    this.fullName = fullName
                    this.email = email
                    this.phoneNumber = phoneNumber
                    this.userId = FireBase.auth.currentUser!!.uid
                }
                pushUserToDatabase(user)
                Tools.startNewActivity(this, DomainActivity(), true)

            } else {
                val dialogTitle: String
                val dialogDesc: String

                when (task.exception) {

                    is FirebaseAuthInvalidCredentialsException -> {
                        dialogTitle = getString(R.string.email_format_exception_title)
                        dialogDesc = getString(R.string.email_format_exception_desc)
                    }
                    else -> {
                        dialogTitle = getString(R.string.firebase_connection_error_title)
                        dialogDesc = getString(R.string.firebase_connection_error_desc)
                    }
                }

                loaderLayout.visibility = View.GONE
                Tools.dialog(
                    this@SignUpActivity,
                    dialogTitle,
                    dialogDesc,
                    Tools.DIALOG_ERROR,
                    getString(R.string.try_again)
                )
            }
        }
    }

    private fun pushUserToDatabase(userModel: UserModel){
        db.collection(FireBase.TABLE_NAME_USERS)
            .add(userModel)
            .addOnSuccessListener {
                Log.d("registration", "User has been added to Database")
            }
            .addOnFailureListener { e ->
                Log.d("registration", "Failed to add User to Database error : ${e.message}")
            }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
}