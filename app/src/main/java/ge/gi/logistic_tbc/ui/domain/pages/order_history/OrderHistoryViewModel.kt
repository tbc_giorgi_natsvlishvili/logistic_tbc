package ge.gi.logistic_tbc.ui.domain.pages.order_history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.firebase.FireBaseCallBack
import ge.gi.logistic_tbc.ui.authorisation.User
import ge.gi.logistic_tbc.ui.domain.models.Order


class OrderHistoryViewModel: ViewModel() {

    private val _order = MutableLiveData<MutableList<Order>>()

    val order: LiveData<MutableList<Order>> = _order

    fun getData(){
        FireBase.getData(
            FireBase.TABLE_NAME_ORDER,
            FireBase.USER_ID,
            FireBase.auth.currentUser!!.uid,
            model =  Order::class.java,
            fireBaseCallBack =  object : FireBaseCallBack {
                override fun <T> response(list: MutableList<T>) {
                    val ordersList: MutableList<Order> = list.filterIsInstance<Order>().toMutableList()
                    ordersList.sortByDescending {
                        it.createOrderDate
                    }
                    _order.value = ordersList
                }
            })
    }
}