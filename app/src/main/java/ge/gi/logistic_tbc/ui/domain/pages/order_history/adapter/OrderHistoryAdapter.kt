package ge.gi.logistic_tbc.ui.domain.pages.order_history.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.databinding.ItemOrderHistoryBinding
import ge.gi.logistic_tbc.ui.domain.models.Order

class OrderHistoryAdapter(
    private val orders: MutableList<Order>,
    private val onOrderClicked: OnOrderClicked,
    private val swipeRefreshLayout: SwipeRefreshLayout,
    private val orderHistoryRV: RecyclerView
) :
    RecyclerView.Adapter<OrderHistoryAdapter.ViewHolder>(), Filterable {

    private val ordersFull = mutableListOf<Order>()

    fun addItemsInOrdersFull(orders: MutableList<Order>){
        ordersFull.clear()
        ordersFull.addAll(orders)
    }
    init {
        orderHistoryRV.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val topRowVerticalPosition =
                    if (orderHistoryRV.childCount == 0) 0 else recyclerView.getChildAt(
                        0
                    ).top
                swipeRefreshLayout.isEnabled = topRowVerticalPosition >= 0
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }

    override fun getItemCount() = orders.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_order_history, parent, false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.applyData()
    }

    inner class ViewHolder(private val binding: ItemOrderHistoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun applyData() {
            val model = orders[adapterPosition]

            binding.order = model

            itemView.setOnClickListener {
                onOrderClicked.onOrderClicked(model.orderID!!)
            }
        }
    }

    override fun getFilter(): Filter {
        return myFilter
    }

    private val myFilter = object : Filter() {
        override fun performFiltering(p0: CharSequence?): FilterResults {
            val filteredList = mutableListOf<Order>()

            if (p0 == null || p0.isEmpty()) {
                filteredList.addAll(ordersFull)
            } else {
                val inputTxt = p0.toString().toLowerCase().trim()

                for (item in ordersFull) {
                    if (item.product.name!!.toLowerCase().contains(inputTxt)) {
                        filteredList.add(item)
                    }
                }
            }

            return FilterResults().apply {
                values = filteredList
            }
        }

        override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
            orders.clear()
            orders.addAll(p1!!.values as MutableList<Order>)
            notifyDataSetChanged()
        }
    }


}