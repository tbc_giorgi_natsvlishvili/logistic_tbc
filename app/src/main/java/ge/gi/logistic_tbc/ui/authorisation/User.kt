package ge.gi.logistic_tbc.ui.authorisation

import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.firebase.FireBaseCallBack
import ge.gi.logistic_tbc.ui.domain.models.UserModel

object User {
    var  userFetched = false
    var documentID = ""
    lateinit var userModel: UserModel

    fun getUserInfo() {
        FireBase.getData(
            FireBase.TABLE_NAME_USERS,
            FireBase.USER_ID,
            FireBase.auth.currentUser!!.uid,
            true,
            UserModel::class.java,
            object : FireBaseCallBack {
                override fun <T> response(list: MutableList<T>) {
                    val userModelList: MutableList<UserModel> =
                        list.filterIsInstance<UserModel>().toMutableList()

                    if (userModelList.isNotEmpty()) {
                        userModel = userModelList[0]
                        userFetched = true
                    }
                }

                override fun getDocumentID(documentID: String) {
                    this@User.documentID = documentID
                }
            }
        )
    }
}