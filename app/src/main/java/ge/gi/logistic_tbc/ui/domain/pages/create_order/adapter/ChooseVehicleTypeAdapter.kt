package ge.gi.logistic_tbc.ui.domain.pages.create_order.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.extensions.setImageFromMipMap
import ge.gi.logistic_tbc.ui.domain.pages.create_order.model.Vehicle
import kotlinx.android.synthetic.main.item_choose_vehicle.view.*

class ChooseVehicleTypeAdapter(
    private val vehicles: MutableList<Vehicle>,
    private val onVehicleClick: OnVehicleClick
) :
    RecyclerView.Adapter<ChooseVehicleTypeAdapter.ViewHolder>() {

    override fun getItemCount() = vehicles.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_choose_vehicle, parent, false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.applyData()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun applyData() {
            val model = vehicles[adapterPosition]

            itemView.vehicleImg.setImageResource(model.image)
            itemView.selectedBG.visibility = setVisibility(model.checked)
            itemView.vehicleTypeTxt.text = model.vehicleType

            itemView.setOnClickListener {
                vehicles.forEach {
                    it.checked = false
                }
                vehicles[adapterPosition].checked = true
                onVehicleClick.onVehicleClick(adapterPosition)
                notifyDataSetChanged()
            }
        }

        private fun setVisibility(isVisible: Boolean) = if (isVisible)
            View.VISIBLE
        else
            View.INVISIBLE
    }


}