package ge.gi.logistic_tbc.ui.domain.pages.create_order

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.network.google_autocomplete.AddressModelClass
import ge.gi.logistic_tbc.network.google_autocomplete.GoogleAutoComplete
import ge.gi.logistic_tbc.network.google_autocomplete.AddressesRecyclerViewAdapter
import ge.gi.logistic_tbc.network.google_autocomplete.OnAddressClickListener
import kotlinx.android.synthetic.main.activity_search_address.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class SearchAddressActivity : AppCompatActivity() {

    lateinit var addressesRecyclerViewAdapter: AddressesRecyclerViewAdapter
    private val addressItems = ArrayList<AddressModelClass>()

    companion object {
        const val ADDRESS_EXTRA_KEY = "address"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_address)

        initToolbar()
        initRV()
        setListener()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitleTV.text = getString(R.string.search_address)
    }

    private fun initRV() {
        addressesRecyclerViewAdapter =
            AddressesRecyclerViewAdapter(
                addressItems,
                onAddressClickCallback
            )
        addressesRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@SearchAddressActivity)
            adapter = addressesRecyclerViewAdapter
        }
    }

    private fun getAutoResponseResponse(input: String) {
        GoogleAutoComplete.getAutoResponseResponse(input, object : retrofit2.Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {}

            override fun onResponse(call: Call<String>, response: Response<String>) {
                val json = JSONObject(response.body().toString())

                if (json.has("predictions")) {
                    val places = json.getJSONArray("predictions")

                    (0 until places.length()).forEach {
                        val place = places[it] as JSONObject
                        if (place.has("description") && place.has("place_id")) {
                            addressItems.add(
                                AddressModelClass(
                                    place.getString("description"),
                                    place.getString("place_id")
                                )
                            )
                        }
                    }
                }

                // Update data to RecyclerView
                addressesRecyclerViewAdapter.notifyDataSetChanged()

            }

        })
    }


    // Text Watcher for address EditText
    private val addressEDTextWatcherListener = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {}

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            // Autocomplete API call
            addressItems.clear()
            getAutoResponseResponse(p0.toString())
        }

    }

    private val onAddressClickCallback = object : OnAddressClickListener {
        override fun onClick(position: Int) {
            val address = addressItems[position]
            intent.putExtra("address", address.description)
            setResult(Activity.RESULT_OK, intent)
            onBackPressed()
        }
    }

    private fun setListener() {
        addressED.addTextChangedListener(addressEDTextWatcherListener)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
}