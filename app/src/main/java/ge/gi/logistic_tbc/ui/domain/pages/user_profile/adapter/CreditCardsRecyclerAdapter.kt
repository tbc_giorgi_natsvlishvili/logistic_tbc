package ge.gi.logistic_tbc.ui.domain.pages.user_profile.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.ui.domain.models.UserModel
import kotlinx.android.synthetic.main.item_credit_card.view.*

class CreditCardsRecyclerAdapter(private val items: MutableList<UserModel.CreditCard>) :
    RecyclerView.Adapter<CreditCardsRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_credit_card, parent, false)
    )


    override fun onBindViewHolder(holder: CreditCardsRecyclerAdapter.ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = items.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var card : UserModel.CreditCard
        fun onBind() {
            card = items[adapterPosition]
            val cardNumber = UserModel.CreditCard.getFormattedCardID(card.cardID!!)

            itemView.cardHolder.text = card.holder
            itemView.cardNumber.text = cardNumber
            itemView.cardExpireDate.text = card.expireDate
        }
    }


}
