package ge.gi.logistic_tbc.ui.rate_driver

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d

import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.tools.OnDialogCallBack
import ge.gi.logistic_tbc.tools.Tools
import ge.gi.logistic_tbc.ui.authorisation.LoginActivity
import ge.gi.logistic_tbc.ui.domain.DomainActivity
import ge.gi.logistic_tbc.ui.domain.pages.order_history.OrderDetailsActivity
import ge.gi.logistic_tbc.ui.rate_driver.adapter.ChooseStarListener
import ge.gi.logistic_tbc.ui.rate_driver.adapter.StarsAdapter
import kotlinx.android.synthetic.main.activity_rate_driver.*

class RateDriverActivity : AppCompatActivity() {

    companion object {
        const val RATE_DRIVER_IMG = "driver_img_url"
        const val RATE_DRIVER_FULL_NAME = "driver_full_name"
    }

    private var reportTxt = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate_driver)

        parseIntent()
        initRV()
        setListeners()

    }

    private var orderID = ""
    private var url = ""
    private var driverName = ""
    private fun parseIntent() {
        if (intent.extras!!.getString(RATE_DRIVER_IMG) != null)
            url = intent.extras!!.getString(RATE_DRIVER_IMG)!!

        if (intent.extras!!.getString(RATE_DRIVER_FULL_NAME) != null)
            driverName = intent.extras!!.getString(RATE_DRIVER_FULL_NAME)!!

        if (intent.extras!!.getString(OrderDetailsActivity.ORDER_ID) != null)
            orderID = intent.extras!!.getString(OrderDetailsActivity.ORDER_ID)!!

        d("notificationTest", " es aris  $url, $driverName , $orderID")
        setData()
    }

    private fun setListeners() {
        reportTextHere.setOnClickListener {
            Tools.commentDialog(this, reportTxt, object : OnDialogCallBack {
                override fun onNoteAdded(text: String) {
                    reportTxt = text
                }
            })
        }

        getOrderNo.setOnClickListener {
            if (getOrderYes.isChecked) {
                getOrderYes.isChecked = false
            }
        }

        getOrderYes.setOnClickListener {
            if (getOrderNo.isChecked) {
                getOrderNo.isChecked = false
            }
        }


        expectedNo.setOnClickListener {
            if (expectedYes.isChecked) {
                expectedYes.isChecked = false
            }
        }

        expectedYes.setOnClickListener {
            if (expectedNo.isChecked) {
                expectedNo.isChecked = false
            }
        }

        addRateBtn.setOnClickListener {
            commitUserRate()
        }
    }

    private fun initRV() {
        startSelectorRV.apply {
            layoutManager =
                LinearLayoutManager(this@RateDriverActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = StarsAdapter(object : ChooseStarListener {
                override fun selectedStar(count: Int) {

                }
            })
        }
    }

    private fun setData() {
        Glide.with(this).load(url).into(driverImg)
        fullName.text = driverName
    }

    private fun commitUserRate() {
        Tools.dialog(
            this@RateDriverActivity,
            getString(R.string.driver_rated),
            getString(R.string.driver_rated_desc),
            Tools.DIALOG_SUCCESS,
            multipleButtons = true,
            onDialogCallBack = object : OnDialogCallBack {
                override fun onYesButtonPressed() {

                    Tools.startNewActivity(this@RateDriverActivity, DomainActivity(), true)
                }

                override fun onNoButtonPressed() {
                    onBackPressed()
                }
            })
    }

}