package ge.gi.logistic_tbc.ui.domain.pages.user_profile

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.firebase.FireBaseCallBack
import ge.gi.logistic_tbc.image_chooser.EasyImage
import ge.gi.logistic_tbc.image_chooser.ImageChooserUtils
import ge.gi.logistic_tbc.image_chooser.MediaFile
import ge.gi.logistic_tbc.image_chooser.MediaSource
import ge.gi.logistic_tbc.tools.Tools
import ge.gi.logistic_tbc.ui.authorisation.User
import ge.gi.logistic_tbc.ui.domain.models.UserModel
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class EditProfileActivity : AppCompatActivity() {

    private var imageFile: MediaFile? = null

    private var mStorageRef: StorageReference? = null

    // User
    lateinit var user: UserModel
    private lateinit var documentId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        initToolbar()
        fetchUserFromDB()
        initUserMetaData()
        setListeners()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitleTV.text = getString(R.string.edit_profile_title)
    }

    private fun initUserMetaData() {
        editEmail.setText(user.email)
        editFullName.setText(user.fullName)
        editPhoneNumber.setText(user.phoneNumber)
        editBillingAddress.setText(user.billingAddress)

//        if (user.imgUrl != null)
//            Glide.with(applicationContext).load(Uri.parse(user.imgUrl)).into(profileIV)
    }

    private fun fetchUserFromDB() {
        if (User.userFetched) {
            user = User.userModel
            documentId = User.documentID
        }
    }

    private fun setListeners() {
        imageChooserBtn.setOnClickListener {
            ImageChooserUtils.choosePhoto(this)
        }

        editProfileBtn.setOnClickListener {
            validateFields()
        }
    }

    private fun validateFields() {
        val email = editEmail.text.toString()
        val fullName = editFullName.text.toString()
        val phoneNumber = editPhoneNumber.text.toString()
        val billingAddress = editBillingAddress.text.toString()

        if (email.isEmpty() || fullName.isEmpty() || phoneNumber.isEmpty() || billingAddress.isEmpty()) {
            Tools.dialog(
                this,
                getString(R.string.fields_is_empty_title),
                getString(R.string.fields_is_empty_desc),
                Tools.DIALOG_ALERT,
                getString(R.string.try_again)
            )
            return
        }

        updateUserProfile(email, fullName, phoneNumber, billingAddress)
    }

    private fun updateUserProfile(
        email: String,
        fullName: String,
        phoneNumber: String,
        billingAddress: String
    ) {
        val params = mutableMapOf<String, Any>()
        params["email"] = email
        params["fullName"] = fullName
        params["phoneNumber"] = phoneNumber
        params["billingAddress"] = billingAddress


        FireBase.auth.currentUser!!.updateEmail(email)

        FireBase.update(
            FireBase.TABLE_NAME_USERS,
            documentId,
            params,
            object : FireBaseCallBack {
                override fun onSuccessfulUpdate() {
                    Tools.dialog(
                        this@EditProfileActivity,
                        getString(R.string.profile_updated),
                        getString(R.string.profile_updated_desc),
                        Tools.DIALOG_SUCCESS,
                        getString(R.string.ok)
                    )
                }

                override fun onFailureUpdate(error: String) {
                    Tools.dialog(
                        this@EditProfileActivity,
                        getString(R.string.profile_update_error),
                        error,
                        Tools.DIALOG_ERROR,
                        getString(R.string.try_again)
                    )
                }
            }
        )

    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageChooserUtils.PERMISSIONS_REQUEST) {
            if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
//                    ImageChooserUtils.chooseResource(this)
                    ImageChooserUtils.easyImage.openGallery(this)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ImageChooserUtils.easyImage.handleActivityResult(
            requestCode,
            resultCode,
            data,
            this,
            object : EasyImage.Callbacks {
                override fun onImagePickerError(error: Throwable, source: MediaSource) {
                }

                override fun onMediaFilesPicked(imageFiles: Array<MediaFile>, source: MediaSource) {
                    if (imageFiles.isNotEmpty()) {
                        imageFile = imageFiles[0]
                        Glide.with(applicationContext).load(imageFile!!.uri.toString())
                            .into(profileIV)
                    }
                }

                override fun onCanceled(source: MediaSource) {
                }
            })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
}