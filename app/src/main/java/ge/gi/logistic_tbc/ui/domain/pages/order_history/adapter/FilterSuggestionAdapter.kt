package ge.gi.logistic_tbc.ui.domain.pages.order_history.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.ui.domain.pages.order_history.model.FilterModel
import kotlinx.android.synthetic.main.item_filter_suggestion.view.*

class FilterSuggestionAdapter(
    private val suggestions: List<FilterModel>,
    private val onFilterSuggestionClickListener: OnFilterSuggestionClickListener
) :
    RecyclerView.Adapter<FilterSuggestionAdapter.ViewHolder>() {


    fun clearAll(){
        suggestions.forEach {
            it.checked = false
        }
        notifyDataSetChanged()
    }

    override fun getItemCount() = suggestions.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_filter_suggestion, parent, false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.applyData()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun applyData() {
            val model = suggestions[adapterPosition]
            itemView.suggestionTV.text = model.title

            if (model.checked) {
                itemView.background =
                    ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.background_filter_suggestion_checked
                    )
            } else {
                itemView.background =
                    ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.background_filter_suggestion_unchecked
                    )
            }

            itemView.setOnClickListener {
                suggestions.forEach {
                    it.checked = false
                }
                model.checked = !model.checked
                notifyDataSetChanged()
                onFilterSuggestionClickListener.onSuggestionClick(model,model.statusCode!!)
            }
        }
    }
}