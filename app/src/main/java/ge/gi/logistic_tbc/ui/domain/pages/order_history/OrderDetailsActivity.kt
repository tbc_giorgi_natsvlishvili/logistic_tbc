package ge.gi.logistic_tbc.ui.domain.pages.order_history

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log.d
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.databinding.ActivityOrderDetailsBinding
import ge.gi.logistic_tbc.extensions.cancelOrderVisibility
import ge.gi.logistic_tbc.extensions.setImageFromMipMap
import ge.gi.logistic_tbc.extensions.viewVisibility
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.firebase.FireBaseCallBack
import ge.gi.logistic_tbc.tools.OnDialogCallBack
import ge.gi.logistic_tbc.tools.Tools
import ge.gi.logistic_tbc.ui.authorisation.LoginActivity
import ge.gi.logistic_tbc.ui.domain.DomainActivity
import ge.gi.logistic_tbc.ui.domain.models.Order
import kotlinx.android.synthetic.main.activity_order_details.*
import kotlinx.android.synthetic.main.item_order_history.*
import kotlinx.android.synthetic.main.toolbar_layout.*


class OrderDetailsActivity : AppCompatActivity() {

    companion object {
        const val ORDER_ID = "orderID"
    }

    private lateinit var orderID: String
    private var order: Order? = null
    private lateinit var binding: ActivityOrderDetailsBinding
    private lateinit var documentId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_details)


        initToolbar()
        parseDataFromIntent()
        fetchDataFromDB()

        setListeners()
    }

    private fun initUI() {
        binding.driverContainer.viewVisibility(order!!.driver.driverFullName)
        binding.productPrice.viewVisibility(order!!.product.price)
        binding.date.viewVisibility(order!!.deliverDate)
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitleTV.text = getString(R.string.order_details)
    }

    private fun doneBtnVisibility() {
        done.cancelOrderVisibility(order!!.orderStatusCode!!)
        done.text = getString(R.string.cancel_order)
    }

    private fun parseDataFromIntent() {
        if (intent.extras!!.getString(ORDER_ID) != null) {
            orderID = intent.extras!!.getString(ORDER_ID)!!
        }

        d("orderIDQWE", orderID)
    }

    private fun setListeners() {
        binding.driverCallBtn.setOnClickListener {
            val intent =
                Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + order!!.driver.driverPhoneNumber))
            startActivity(intent)
        }
        done.setOnClickListener {
            Tools.dialog(
                this@OrderDetailsActivity,
                getString(R.string.driver_rated),
                getString(R.string.driver_rated_desc),
                Tools.DIALOG_SUCCESS,
                multipleButtons = true,
                onDialogCallBack = object : OnDialogCallBack {
                    override fun onYesButtonPressed() {
                        updateDataBase()
                    }
                })
        }
    }

    private fun fetchDataFromDB() {
        FireBase.getData(
            FireBase.TABLE_NAME_ORDER,
            FireBase.ORDER_ID,
            orderID,
            true,
            Order::class.java,
            object : FireBaseCallBack {
                override fun <T> response(list: MutableList<T>) {
                    val orderList: MutableList<Order> =
                        list.filterIsInstance<Order>().toMutableList()
                    order = orderList[0]

                    binding.order = order

                    doneBtnVisibility()
                    setData()
                    initUI()
                }

                override fun getDocumentID(documentID: String) {
                    documentId = documentID
                }
            }
        )
    }

    private fun updateDataBase() {
        val params = mutableMapOf<String, Any>()
        params["orderStatus"] = getString(R.string.canceled_by_user)
        params["orderStatusCode"] = 3

        FireBase.update(
            FireBase.TABLE_NAME_ORDER,
            documentId,
            params,
            object : FireBaseCallBack {
                override fun onSuccessfulUpdate() {
                    fetchDataFromDB()
                    d("UpdateFireStore", "Success")
                }

                override fun onFailureUpdate(error: String) {
                    d("UpdateFireStore", error)
                }
            }
        )
    }

    private fun setData() {
        if (order!!.orderStatusCode != null) {
            statusIC.setImageFromMipMap(order!!.orderStatusCode!!)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
}