package ge.gi.logistic_tbc.ui.domain.pages.create_order.model

class Vehicle(
    val image: Int,
    var vehicleType: String,
    var checked: Boolean = false
)