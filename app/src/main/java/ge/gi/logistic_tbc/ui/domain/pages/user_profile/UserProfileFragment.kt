package ge.gi.logistic_tbc.ui.domain.pages.user_profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.databinding.FragmentUserProfileBinding
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.firebase.FireBaseCallBack
import ge.gi.logistic_tbc.tools.OnDialogCallBack
import ge.gi.logistic_tbc.tools.Tools
import ge.gi.logistic_tbc.ui.authorisation.LoginActivity
import ge.gi.logistic_tbc.ui.authorisation.User
import ge.gi.logistic_tbc.ui.domain.DomainActivity
import ge.gi.logistic_tbc.ui.domain.models.UserModel
import ge.gi.logistic_tbc.ui.domain.pages.user_profile.adapter.CreditCardsRecyclerAdapter


class UserProfileFragment : Fragment() {

    companion object {
        const val ADD_CARD_REQUEST_CODE = 11
        const val IS_CARD_ADDED = "isCardAdded"
    }

    private lateinit var binding: FragmentUserProfileBinding
    private lateinit var userModel: UserModel
    private lateinit var creditCardsRecyclerAdapter: CreditCardsRecyclerAdapter
    private val creditCardsList = mutableListOf<UserModel.CreditCard>()

    lateinit var documentID: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(container!!.context),
            R.layout.fragment_user_profile,
            container,
            false
        )
        recyclerInit()
        fetchUserFromDB()
        setListeners()
        return binding.root
    }

    private fun initUI() {
        creditCardsList.clear()
        if (userModel.creditCard != null)
            creditCardsList.addAll(userModel.creditCard!!.toList())
        creditCardsRecyclerAdapter.notifyDataSetChanged()

        binding.user = userModel
        if (userModel.imgUrl != null){
            Glide.with(context!!).load(userModel.imgUrl).into(binding.profilePictureIV)
        }
    }

    private fun recyclerInit() {
        creditCardsRecyclerAdapter = CreditCardsRecyclerAdapter(creditCardsList)

        binding.creditCardsRecyclerView.layoutManager = LinearLayoutManager(activity)
        binding.creditCardsRecyclerView.adapter = creditCardsRecyclerAdapter
    }

    private fun fetchUserFromDB() {
        FireBase.getData(
            FireBase.TABLE_NAME_USERS,
            FireBase.USER_ID,
            FireBase.auth.currentUser!!.uid,
            true,
            UserModel::class.java,
            object : FireBaseCallBack {
                override fun <T> response(list: MutableList<T>) {
                    val userModelList: MutableList<UserModel> =
                        list.filterIsInstance<UserModel>().toMutableList()
                    userModel = userModelList[0]

                    initUI()
                }

                override fun getDocumentID(documentID: String) {
                    this@UserProfileFragment.documentID = documentID
                }
            }
        )
    }

    private fun setListeners() {
        binding.addCreditCardBtn.setOnClickListener {
            val intent = Intent(activity as DomainActivity, AddCreditCardActivity::class.java)
            intent.putExtra(FireBase.DOCUMENT_ID, documentID)
            startActivityForResult(intent, ADD_CARD_REQUEST_CODE)
            (activity as DomainActivity).overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }

        binding.logoutBtn.setOnClickListener {
            Tools.dialog(
                context!!,
                getString(R.string.sign_out_title),
                getString(R.string.sign_out_desc),
                Tools.DIALOG_ALERT,
                multipleButtons = true,
                onDialogCallBack = object : OnDialogCallBack {
                    override fun onYesButtonPressed() {
                        Tools.startNewActivity((activity as DomainActivity), LoginActivity(), true)
                        FireBase.auth.signOut()
                    }
                })
        }

        binding.editProfileBtn.setOnClickListener {
            Tools.startNewActivity(activity as DomainActivity, EditProfileActivity(), false)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    ADD_CARD_REQUEST_CODE -> {
                        if (data != null) {
                            val boolean = data.extras!!.getBoolean(IS_CARD_ADDED, false)
                            if (boolean)
                                fetchUserFromDB()
                            data.removeExtra(IS_CARD_ADDED)
                        }
                    }
                }
            }

            Activity.RESULT_CANCELED -> {
                //nothing has changed
            }
        }
    }
}