package ge.gi.logistic_tbc.ui.rate_driver.adapter

interface ChooseStarListener {
    fun selectedStar(count : Int)
}