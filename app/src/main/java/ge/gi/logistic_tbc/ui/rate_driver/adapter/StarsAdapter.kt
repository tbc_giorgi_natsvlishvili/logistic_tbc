package ge.gi.logistic_tbc.ui.rate_driver.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.ui.rate_driver.model.Star
import kotlinx.android.synthetic.main.item_start.view.*

class StarsAdapter(private val chooseStarListener: ChooseStarListener) :
    RecyclerView.Adapter<StarsAdapter.ViewHolder>() {

    private val starts = listOf(Star(), Star(), Star(), Star(), Star())

    override fun getItemCount() = starts.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_start, parent, false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.applyData()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun applyData() {
            if (starts[adapterPosition].isYellow)
                itemView.star.setImageResource(R.mipmap.star_selected)
            else
                itemView.star.setImageResource(R.mipmap.star_unseleckted)

            itemView.star.setOnClickListener {
                chooseStarListener.selectedStar(adapterPosition + 1)

                starts.forEach {
                    it.isYellow = false
                }

                for (i in 0..adapterPosition) {
                    starts[i].isYellow = true
                }
                notifyDataSetChanged()
            }

        }
    }


}