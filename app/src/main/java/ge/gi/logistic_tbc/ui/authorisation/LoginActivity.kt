package ge.gi.logistic_tbc.ui.authorisation

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.extensions.appendColorfulText
import ge.gi.logistic_tbc.extensions.chooseDrawableByEditTextState
import ge.gi.logistic_tbc.extensions.isEmailValid
import ge.gi.logistic_tbc.extensions.setPasswordVisibility
import ge.gi.logistic_tbc.firebase.FireBase
import ge.gi.logistic_tbc.tools.Tools
import ge.gi.logistic_tbc.ui.domain.DomainActivity
import ge.gi.logistic_tbc.ui.domain.models.UserModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.layout_loader.*

class LoginActivity : AppCompatActivity() {

    companion object {
        const val RC_SIGN_IN = 1
        const val REGISTER = 2
    }

    private lateinit var googleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initUI()
        initFirebase()
        listeners()

    }

    private fun listeners() {
        signUpTV.setOnClickListener {
            Tools.startNewActivity(this, SignUpActivity())
        }

        emailE.isEmailValid(emailSuccessIV)

//      EditText Eye Logic Handling
//      ************************
        passwordToggleImageView.setOnClickListener {
            passwordE.tag = if (passwordE.tag == "1") "0" else "1"
            passwordToggleImageView.chooseDrawableByEditTextState(passwordE.isFocused, passwordE)
            passwordE.setPasswordVisibility()
        }
        passwordE.setOnFocusChangeListener { view, b ->
            passwordToggleImageView.chooseDrawableByEditTextState(b, view as EditText)
        }
//        ***********************


        emailLoginBtn.setOnClickListener {
            validateFields()
        }

        signInWithGoogleBtn.setOnClickListener {
            signInWithGoogle()
        }
    }

    private fun initUI() {
        signUpTV.appendColorfulText(
            "${getString(R.string.new_user)} ",
            ContextCompat.getColor(this, R.color.textColor)
        )
        signUpTV.appendColorfulText(
            getString(R.string.sign_up),
            ContextCompat.getColor(this, R.color.colorPrimary)
        )
        signUpTV.appendColorfulText(
            " ${getString(R.string.here)}",
            ContextCompat.getColor(this, R.color.textColor)
        )
    }

    private fun initFirebase() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun validateFields() {
        val email = emailE.text.toString()
        val password = passwordE.text.toString()

        if (email.isEmpty() || password.isEmpty()) {
            Tools.dialog(
                this@LoginActivity,
                getString(R.string.fields_is_empty_title),
                getString(R.string.fields_is_empty_desc),
                Tools.DIALOG_ALERT,
                getString(R.string.try_again)
            )
            return
        }
        if (emailE.tag == "0") {
            Tools.dialog(
                this@LoginActivity,
                getString(R.string.email_is_not_valid_title),
                getString(R.string.email_is_not_valid_desc),
                Tools.DIALOG_ERROR,
                getString(R.string.try_again)
            )
            return
        }
        if (password.length < 6) {
            Tools.dialog(
                this@LoginActivity,
                getString(R.string.password_is_not_valid_title),
                getString(R.string.password_is_not_valid_desc),
                Tools.DIALOG_ERROR,
                getString(R.string.try_again)
            )
            return
        }

        signInWithEmail(email, password)
    }

    private fun signInWithGoogle() {
        startActivityForResult(googleSignInClient.signInIntent, RC_SIGN_IN)
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        loaderLayout.visibility = View.VISIBLE
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        FireBase.auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->

                //ამოწმებს თუ user ი პირველად შედის firestore ში ქმნის უსერისთვის შესაბამის field ებს
                if (task.result!!.additionalUserInfo!!.isNewUser)
                    pushUserToDB()

                if (task.isSuccessful) {
                    val user = FireBase.auth.currentUser
                    updateAuthInfo(user)
                } else {
                    updateAuthInfo(null)
                }
            }
    }

    private fun pushUserToDB() {
        val user = UserModel().apply {
            this.fullName = FireBase.auth.currentUser!!.displayName
            this.email = FireBase.auth.currentUser!!.email
            this.phoneNumber = FireBase.auth.currentUser!!.phoneNumber
            this.userId = FireBase.auth.currentUser!!.uid
        }

        val db = FirebaseFirestore.getInstance()
        db.collection(FireBase.TABLE_NAME_USERS)
            .add(user)
            .addOnSuccessListener {
                d("registration", "User has been added to Database")
            }
            .addOnFailureListener { e ->
                d("registration", "Failed to add User to Database error : ${e.message}")
            }
    }

    private fun updateAuthInfo(user: FirebaseUser?) {
        loaderLayout.visibility = View.GONE
        if (user != null) {
            Tools.startNewActivity(this, DomainActivity(), true)
        }
    }

    private fun signInWithEmail(email: String, password: String) {
        loaderLayout.visibility = View.VISIBLE
        FireBase.auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                loaderLayout.visibility = View.GONE
                Tools.startNewActivity(this, DomainActivity(), true)
            } else {

                val dialogTitle: String
                val dialogDesc: String
                when (task.exception) {
                    is FirebaseAuthInvalidCredentialsException -> {
                        dialogTitle = getString(R.string.password_is_incorrect_title)
                        dialogDesc = getString(R.string.password_is_incorrect_desc)
                    }
                    is FirebaseAuthInvalidUserException -> {
                        dialogTitle = getString(R.string.email_is_incorrect_title)
                        dialogDesc = getString(R.string.email_is_incorrect_desc)
                    }
                    else -> {
                        dialogTitle = getString(R.string.firebase_connection_error_title)
                        dialogDesc = getString(R.string.firebase_connection_error_desc)
                    }
                }
                loaderLayout.visibility = View.GONE
                Tools.dialog(
                    this@LoginActivity,
                    dialogTitle,
                    dialogDesc,
                    Tools.DIALOG_ERROR,
                    getString(R.string.try_again)
                )
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        /**
         * User ის დარეგისტრირება ბაზაში როდესაც Googleით რეგისტრირდება
         */

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)!!
                firebaseAuthWithGoogle(account.idToken!!)

                val acc = GoogleSignIn.getLastSignedInAccount(this@LoginActivity)

            } catch (e: ApiException) {

            }
        } else if (requestCode == REGISTER) {
            if (data != null) {
                startActivity(Intent(applicationContext, DomainActivity::class.java))
            }
        }
    }
}

