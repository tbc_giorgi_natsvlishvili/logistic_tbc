package ge.gi.logistic_tbc.firebase

import android.util.Log.d
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import ge.gi.logistic_tbc.ui.domain.models.UserModel

object FireBase {

    const val TABLE_NAME_ORDER = "order"
    const val TABLE_NAME_USERS = "Users"

    //filter title
    const val USER_ID = "userId"
    const val ORDER_ID = "orderID"
    const val CREDIT_CARD = "creditCard"

    // intent extra keys
    const val DOCUMENT_ID = "document_id"


    val auth = FirebaseAuth.getInstance()

    val db = FirebaseFirestore.getInstance()

    fun <T : Any> getData(
        tableName: String,
        filterName: String,
        filterValue: String,
        needDocumentId: Boolean = false,
        model: Class<T>,
        fireBaseCallBack: FireBaseCallBack
    ) {
//        .orderBy("createOrderDate", Query.Direction.DESCENDING)
        db.collection(tableName)
            .whereEqualTo(filterName, filterValue)
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    val modelsList = mutableListOf<T>()


                    for (document in task.result!!) {

                        if (needDocumentId) {
                            fireBaseCallBack.getDocumentID(document.id)
                        }
                        modelsList.add(document.toObject(model))
                    }

                    fireBaseCallBack.response(modelsList)

                } else {
                    d("------", "failed")
                }
            }
    }

    fun update(
        tableName: String,
        documentId: String,
        params: MutableMap<String,Any>,
        fireBaseCallBack: FireBaseCallBack
    ) {
        val collection = db.collection(tableName).document(documentId)
        collection.update(params)
            .addOnCompleteListener { task ->

                if (task.isSuccessful)
                    fireBaseCallBack.onSuccessfulUpdate()
            }
            .addOnFailureListener {
                fireBaseCallBack.onFailureUpdate(it.message.toString())
            }

    }


    fun addCreditCard(
        tableName: String,
        documentId: String,
        filterName: String,
        filterValue: UserModel.CreditCard,
        fireBaseCallBack: FireBaseCallBack
    ) {
        val collection = db.collection(tableName).document(documentId)
        collection.update(filterName, FieldValue.arrayUnion(filterValue))
            .addOnCompleteListener { task ->

                if (task.isSuccessful)
                    fireBaseCallBack.onSuccessfulUpdate()
            }
            .addOnFailureListener {
                fireBaseCallBack.onFailureUpdate(it.message.toString())
            }

    }

}