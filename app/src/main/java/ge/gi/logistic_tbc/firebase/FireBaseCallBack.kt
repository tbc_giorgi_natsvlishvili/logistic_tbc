package ge.gi.logistic_tbc.firebase

interface FireBaseCallBack {
    fun <T> response(list : MutableList<T>){}
    fun getDocumentID(documentID : String){}

    fun onSuccessfulUpdate(){}

    fun onFailureUpdate(error : String){}

}