package ge.gi.logistic_tbc.image_chooser

enum class ChooserType {
    CAMERA_AND_GALLERY, CAMERA_AND_DOCUMENTS
}