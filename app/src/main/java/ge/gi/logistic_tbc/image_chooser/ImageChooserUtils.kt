package ge.gi.logistic_tbc.image_chooser

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import ge.gi.logistic_tbc.App

class ImageChooserUtils {
    companion object {
        const val PERMISSIONS_REQUEST = 11

        private fun hasReadExternalStorage() = ActivityCompat.checkSelfPermission(
            App.instance.context(),
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED

        private fun hasWriteExternalStorage() = ActivityCompat.checkSelfPermission(
            App.instance.context(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED


        private fun requestPermissions(activity: Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ), PERMISSIONS_REQUEST
                )
            }
        }


        val easyImage by lazy {
            EasyImage.Builder(App.instance.context())
                .setCopyImagesToPublicGalleryFolder(false)
                .setFolderName("EasyImage sample")
                .allowMultiple(false)
                .build()
        }

        fun choosePhoto(activity: Activity){
            if (hasReadExternalStorage() && hasWriteExternalStorage()){
                easyImage.openGallery(activity)
            }

            else{
                requestPermissions(activity)
            }

        }
    }
}