package ge.gi.logistic_tbc.image_chooser

enum class MediaSource {
    GALLERY, DOCUMENTS, CAMERA_IMAGE, CAMERA_VIDEO, CHOOSER
}