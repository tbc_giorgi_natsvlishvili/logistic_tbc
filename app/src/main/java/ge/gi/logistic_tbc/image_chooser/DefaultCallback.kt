package com.jemalashop.app.image_chooser

import ge.gi.logistic_tbc.image_chooser.EasyImage
import ge.gi.logistic_tbc.image_chooser.MediaSource

/**
 * Stas Parshin
 * 05 November 2015
 */
abstract class DefaultCallback : EasyImage.Callbacks {

    override fun onImagePickerError(error: Throwable, source: MediaSource) {}

    override fun onCanceled(source: MediaSource) {}
}
