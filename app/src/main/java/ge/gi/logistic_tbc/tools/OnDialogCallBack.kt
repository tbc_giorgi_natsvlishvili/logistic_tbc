package ge.gi.logistic_tbc.tools


interface OnDialogCallBack {
    fun okButtonPressed(){}
    fun onYesButtonPressed(){}
    fun onNoButtonPressed(){}
    fun onNoteAdded(text: String){}
}