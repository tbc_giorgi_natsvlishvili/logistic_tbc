package ge.gi.logistic_tbc.tools

import android.app.Activity
import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.util.Patterns
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.ContextCompat
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.extensions.changeDrawableBackgroundColor
import kotlinx.android.synthetic.main.layout_dialog.*
import kotlinx.android.synthetic.main.layout_dialog_comment.*

object Tools {

    const val DIALOG_SUCCESS = 1
    const val DIALOG_ERROR = 2
    const val DIALOG_ALERT = 3

    fun isEmailValid(text: String) = Patterns.EMAIL_ADDRESS.matcher(text).matches()

    fun startNewActivity(
        activity: Activity,
        destinationActivity: Activity,
        clearStack: Boolean = false
    ) {
        val intent = Intent(activity, destinationActivity::class.java)
        if (clearStack)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        activity.startActivity(intent)
        activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    fun dialog(
        context: Context,
        title: String,
        description: String,
        dialogIdentifier: Int,
        buttonText: String = "OK",
        multipleButtons: Boolean = false,
        onDialogCallBack: OnDialogCallBack? = null
    ) {
        val dialog = Dialog(context)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
//        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.layout_dialog)

        val params: ViewGroup.LayoutParams = dialog.window!!.attributes

        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as WindowManager.LayoutParams

        dialog.dialogTitle.text = title
        dialog.dialogDesc.text = description

        if (multipleButtons) {
            dialog.twoBtnContainer.visibility = View.VISIBLE
            dialog.dialogBtnNo.changeDrawableBackgroundColor(Color.RED)
        } else {
            dialog.dialogButton.text = buttonText
            dialog.dialogButton.visibility = View.VISIBLE
        }

        val color: Int

        when (dialogIdentifier) {
            DIALOG_SUCCESS -> {
                color = ContextCompat.getColor(context, R.color.dialogSuccessAccentColor)
                dialog.dialogImageView.setImageResource(R.mipmap.dialog_illustrator_success)
            }
            DIALOG_ALERT -> {
                color = ContextCompat.getColor(context, R.color.dialogAlertAccentColor)
                dialog.dialogImageView.setImageResource(R.mipmap.dialog_illustrator_alert)
            }
            DIALOG_ERROR -> {
                color = ContextCompat.getColor(context, R.color.dialogErrorAccentColor)
                dialog.dialogImageView.setImageResource(R.mipmap.dialog_illustrator_error)
            }
            else -> {
                color = ContextCompat.getColor(context, android.R.color.holo_green_dark)
            }
        }

        dialog.dialogButton.changeDrawableBackgroundColor(color)

        dialog.dialogButton.setOnClickListener {
            onDialogCallBack?.okButtonPressed()

            dialog.cancel()
        }

        dialog.dialogBtnYes.setOnClickListener {
            onDialogCallBack!!.onYesButtonPressed()
            dialog.dismiss()

        }

        dialog.dialogBtnNo.setOnClickListener {
            onDialogCallBack!!.onNoButtonPressed()
            dialog.cancel()
        }



        dialog.show()
    }

    fun commentDialog(
        context: Context,
        text: String,
        onDialogCallBack: OnDialogCallBack
    ) {
        val dialog = Dialog(context)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(R.layout.layout_dialog_comment)

        val params: ViewGroup.LayoutParams = dialog.window!!.attributes

        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as WindowManager.LayoutParams

        dialog.discard.setOnClickListener {
            dialog.cancel()
        }

        dialog.input.setText(text)

        dialog.save.setOnClickListener {
            val input = dialog.input.text.toString()
            onDialogCallBack.onNoteAdded(input)
            dialog.cancel()
        }

        dialog.show()
    }

    fun cancelNotification(context: Context, notificationID: Int) {
        val notificationService = Context.NOTIFICATION_SERVICE
        val nMgr = context.getSystemService(notificationService) as NotificationManager
        nMgr.cancel(notificationID)
    }
}