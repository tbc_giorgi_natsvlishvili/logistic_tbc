package ge.gi.logistic_tbc.extensions

import android.view.View


fun View.viewVisibility(attribute : Any?) {
    visibility = if (attribute != null)
        View.VISIBLE
    else
        View.GONE
}