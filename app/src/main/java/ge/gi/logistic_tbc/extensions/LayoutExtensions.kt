package ge.gi.logistic_tbc.extensions

import android.view.View
import android.widget.FrameLayout

fun FrameLayout.refreshing(isRefreshing: Boolean) {
    visibility = if (isRefreshing) View.VISIBLE else View.GONE
}