package ge.gi.logistic_tbc.extensions

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import ge.gi.logistic_tbc.tools.Tools
import kotlinx.android.synthetic.main.activity_login.*

fun EditText.isEmailValid(successImage: ImageView) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            if (Tools.isEmailValid(p0.toString())) {
                successImage.visibility = View.VISIBLE
                tag = "1"
            } else {
                successImage.visibility = View.INVISIBLE
                tag = "0"
            }

        }
    })
}

fun EditText.setPasswordVisibility() {
    inputType =
        if (tag == "1") InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD else InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
    setSelection(length())
}

