package ge.gi.logistic_tbc.extensions

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import ge.gi.logistic_tbc.R

fun TextView.appendColorfulText(text: String, color: Int) {
    val spannable = SpannableString(text)
    spannable.setSpan(
        ForegroundColorSpan(color),
        0,
        spannable.length,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    append(spannable)
}

@BindingAdapter("cancelOrderVisibility")
fun TextView.cancelOrderVisibility(statusCode: Int) {

    visibility = if (statusCode == 1) {
        View.VISIBLE
    } else {
        View.GONE
    }
}

fun TextView.setColorByEditTextState(isFocused: Boolean) {
    if (isFocused) {
        setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
    }else{
        setTextColor(ContextCompat.getColor(context, R.color.textColor))
    }
}

