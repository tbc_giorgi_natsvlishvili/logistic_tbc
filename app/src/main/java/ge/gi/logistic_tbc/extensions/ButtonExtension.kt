package ge.gi.logistic_tbc.extensions

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Build
import android.widget.Button

fun Button.changeDrawableBackgroundColor(color : Int) {
    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        background.colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
    } else {
        @Suppress("DEPRECATION")
        background.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
    }
}