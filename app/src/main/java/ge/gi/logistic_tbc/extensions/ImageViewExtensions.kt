package ge.gi.logistic_tbc.extensions

import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import ge.gi.logistic_tbc.R

fun ImageView.chooseDrawableByEditTextState(boolean: Boolean, editText: EditText) {
    if(boolean){
        if(editText.tag == "0")
            setImageResource(R.mipmap.ic_password_toggle_show_blue)
        else
            setImageResource(R.mipmap.ic_password_toggle_hide_blue)
    }else {
        if (editText.tag == "1")
            setImageResource(R.mipmap.ic_password_toggle_hide_grey)
        else
            setImageResource(R.mipmap.ic_password_toggle_show_grey)
    }
}

@BindingAdapter("setImageFromMipMap")
fun ImageView.setImageFromMipMap(statusCode: Int){

    when (statusCode) {
        0 -> setImageResource(R.mipmap.ic_shipped)
        1 ->  setImageResource(R.mipmap.ic_pending)
        2 ->  setImageResource(R.mipmap.ic_on_the_way)
        3 ->  setImageResource(R.mipmap.ic_canceled)
        else ->  setImageResource(R.mipmap.ic_canceled)
    }

}

@BindingAdapter("setImageUrl")
fun CircleImageView.setImageUrl(url : String?) {
    if(url != null)
        Glide.with(context).load(url).centerCrop().into(this)
}