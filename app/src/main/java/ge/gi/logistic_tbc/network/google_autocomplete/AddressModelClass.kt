package ge.gi.logistic_tbc.network.google_autocomplete

data class AddressModelClass(
    val description : String,
    val placeId : String
)