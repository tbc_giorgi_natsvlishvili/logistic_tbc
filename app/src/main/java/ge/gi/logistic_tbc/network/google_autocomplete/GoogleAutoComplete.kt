package ge.gi.logistic_tbc.network.google_autocomplete

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap

object GoogleAutoComplete {

    private const val BASE_URL = "https://maps.googleapis.com/maps/api/place/"
    private const val API_KEY = "AIzaSyBADWUmhO9XNVF_-qSZR6RQWcoHfSpAr6E"

    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(BASE_URL)
        .build()


    private var service = retrofit.create<ApiService>(
        ApiService::class.java
    )


    fun getAutoResponseResponse(inputString: String, callback: Callback<String>) {
        val parameters = HashMap<String, String>()
        parameters["input"] = inputString
        parameters["key"] = API_KEY

        // Depends on the country API response
        parameters["language"] = "en"
        parameters["components"] = "country:GE"
        service.getAutoResponseResponse(parameters).enqueue(callback)
    }

    interface ApiService {

        @GET("autocomplete/json?")
        fun getAutoResponseResponse(
            @QueryMap parameters: HashMap<String, String>
        ): Call<String>
    }
}