package ge.gi.logistic_tbc.network.google_autocomplete

interface OnAddressClickListener {
    fun onClick(position : Int)
}