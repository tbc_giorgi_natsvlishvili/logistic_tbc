package ge.gi.logistic_tbc.network.google_autocomplete

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ge.gi.logistic_tbc.R
import kotlinx.android.synthetic.main.item_address.view.*

class AddressesRecyclerViewAdapter(
    private val items: ArrayList<AddressModelClass>,
    val onAddressClickListener: OnAddressClickListener
) :
    RecyclerView.Adapter<AddressesRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_address, parent, false)
    )


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = items.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {
            val address = items[adapterPosition]
            itemView.addressTitleTV.text = address.description

            itemView.setOnClickListener {
                onAddressClickListener.onClick(adapterPosition)
            }
        }
    }
}
