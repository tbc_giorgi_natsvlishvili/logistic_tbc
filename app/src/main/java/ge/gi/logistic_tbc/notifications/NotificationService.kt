package ge.gi.logistic_tbc.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.util.Log.d
import androidx.core.app.NotificationCompat
import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult
import ge.gi.logistic_tbc.R
import ge.gi.logistic_tbc.ui.domain.pages.order_history.OrderDetailsActivity
import ge.gi.logistic_tbc.ui.rate_driver.RateDriverActivity

class NotificationService : NotificationExtenderService() {
    companion object {
        const val ORDER_CHANNEL_ID = "11"
        const val RATE_CHANNEL_ID = "12"

        const val NOTIFICATION_TYPE_ORDER = "orderDetail"
        const val NOTIFICATION_TYPE_RATE = "rateDriver"
    }

    private lateinit var intent: Intent
    private lateinit var notificationManager: NotificationManager
    override fun onNotificationProcessing(receivedResult: OSNotificationReceivedResult): Boolean {
        Log.d("___notification", receivedResult.payload.additionalData.toString())
        val json = receivedResult.payload.additionalData

        val title = json.getString("title")
        val content = json.getString("content")
        val orderID = json.getString("orderID")
        val driverImg = json.getString("driver_img_url")
        val driverFullImg = json.getString("driver_full_name")
        d("orderIDQWE", orderID)

        if (json.getString("type") == NOTIFICATION_TYPE_ORDER) {
            intent = Intent(this, OrderDetailsActivity::class.java).apply {
                action = "actionstring" + System.currentTimeMillis()
                putExtra("orderID", orderID)
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
        } else {
            intent = Intent(this, RateDriverActivity::class.java).apply {
                action = "actionstring" + System.currentTimeMillis()
                putExtra("orderID", orderID)
                putExtra("driver_img_url", driverImg)
                putExtra("driver_full_name", driverFullImg)
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
        }

        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val builder = NotificationCompat.Builder(this, ORDER_CHANNEL_ID)
            .setSmallIcon(R.mipmap.logistics)
            .setContentTitle(title)
            .setAutoCancel(true)
            .setContentText(content)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)


        if (json.getString("type") == NOTIFICATION_TYPE_ORDER) {
            notificationManager = createNotificationChannel(
                getString(R.string.orders_notification_channel),
                getString(R.string.orders_notification_channel),
                ORDER_CHANNEL_ID
            )
        } else {
            notificationManager = createNotificationChannel(
                getString(R.string.rate_notification_channel),
                getString(R.string.rate_notification_channel),
                RATE_CHANNEL_ID
            )
        }

        notificationManager.notify(1, builder.build())
        return false
    }

    private fun createNotificationChannel(
        name: String,
        descriptionText: String,
        id: String
    ): NotificationManager {

        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val name = getString(R.string.orders_notification_channel)
//            val descriptionText = getString(R.string.orders_notification_channel)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(id, name, importance).apply {
                description = descriptionText
            }
            notificationManager.createNotificationChannel(channel)
        }
        return notificationManager
    }
}