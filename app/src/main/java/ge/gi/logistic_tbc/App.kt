package ge.gi.logistic_tbc

import android.app.Application
import android.content.Context
import com.onesignal.OneSignal

class App : Application() {
    companion object {
        lateinit var instance: App
        private lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
        initOneSignal()
    }

    fun context() = context

    private fun initOneSignal() {
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init();
    }
}