﻿# Logistic - delivery app
delivery app build with Kotlin for Android.

## Requirements:
 - Device with Android system
 - Minimum of SDK version 21.

### Table of contents
* [General info](#general-info)
* [External Libraries](#external-libraries)
* [Functionality](#functionality)
* [App Screens](#app-screens)

## General info
Delivery app build with Kotlin for Android. The app is developed through Advanced Android Development Bootcamp. Since, Logistic is for delivery services, a lot of companies can use it for B2B or even B2C. This app can encourage businesses, make them to get rid of their paper work and simplify their workflow.
	
## External Libraries
Project is created with:
* Firebase Authentication

* Firebase FireStore

* Retrofit for Google Places Autocomplete API

* Glide and CircleImageView for images

* One Signal for Push Notifications
	
## Functionality
### Authentication
- User can register and sign in with both email address and Google account.
- User can upload own photo to Profile and update it's data.
### Order Creation
- Most importantly, User can commit order with name, quantity, weight and Starting and Ending location endpoints. Also, User have ability to choose from different kind of courrier vehicles : Scooter, Mini, Mini+, Heavy.
- User can have Multiple Credit Cards attached to his/her account and choose the right one for specific order by pressing 'Choose credit card'.
### History
- We have pretty amazing feature that encourages users to see their Order History, possibility to filter by product name, unique order id and even by order status.
- User can also Cancel the order (While the order is in 'Pending' process)
- ability to see detailed information about order and even about the driver who is attached to this order.

### Push Notifications
- Push notifications are used to inform user when driver accepts order and he/she is ahead to deliver. user is informed once again when drivers finishes his/her order. User also have ability to rate the driver and give us feedback.

### App Screens

![picture](https://i.imgur.com/oeDBWNQ.jpg)
![picture](https://i.imgur.com/5EzZYSz.jpg)
![picture](https://i.imgur.com/fb4axxS.jpg)
![picture](https://i.imgur.com/Ymk1n5J.jpg)
